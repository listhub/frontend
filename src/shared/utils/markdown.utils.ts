import { ListItem } from '@/shared/types.ts';

export function generateMarkdownList(list: ListItem): string {
  let markdownList = '';

  function generateTaskMarkdown(list: ListItem, depth: number) {
    const checkbox = list.isDone ? '[x]' : '[ ]';
    const indentation = '  '.repeat(depth);
    const tags = list.tags.map(tag => `#${tag.title}`).join(' ');

    markdownList += `${indentation}- ${checkbox} ${list.title} ${tags}\n`;

    if (list.children.length > 0) {
      list.children.forEach(child => generateTaskMarkdown(child, depth + 1));
    }
  }

  generateTaskMarkdown(list, 0);

  return markdownList;
}
