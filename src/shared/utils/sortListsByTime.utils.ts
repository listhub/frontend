import { ListItem } from '../types';

export function sortTasksByTime(tasks: ListItem[] | undefined | null) {
  if (!tasks) return;

  const now = new Date();

  const categories: { [key: string]: ListItem[] } = {
    просроченные: [],
    сегодня: [],
    завтра: [],
    'на этой неделе': [],
    'на следующей неделе': [],
    'в этом месяце': [],
    'в следующем месяце': [],
    'в этом году': [],
    'в следующем году и более': [],
  };

  tasks.forEach(task => {
    const presetDate: string | null = task.presetDate;
    if (!presetDate) return;

    const taskDate = new Date(presetDate);
    const diff = taskDate.getTime() - now.getTime();

    if (diff < 0) {
      categories['просроченные'].push(task);
    } else if (
      taskDate.getDate() === now.getDate() &&
      taskDate.getMonth() === now.getMonth() &&
      taskDate.getFullYear() === now.getFullYear()
    ) {
      categories['сегодня'].push(task);
    } else if (
      taskDate.getDate() === now.getDate() + 1 &&
      taskDate.getMonth() === now.getMonth() &&
      taskDate.getFullYear() === now.getFullYear()
    ) {
      categories['завтра'].push(task);
    } else if (diff < 7 * 24 * 60 * 60 * 1000) {
      categories['на этой неделе'].push(task);
    } else if (diff < 14 * 24 * 60 * 60 * 1000) {
      categories['на следующей неделе'].push(task);
    } else if (
      taskDate.getMonth() === now.getMonth() &&
      taskDate.getFullYear() === now.getFullYear()
    ) {
      categories['в этом месяце'].push(task);
    } else if (
      taskDate.getMonth() === now.getMonth() + 1 &&
      taskDate.getFullYear() === now.getFullYear()
    ) {
      categories['в следующем месяце'].push(task);
    } else if (taskDate.getFullYear() === now.getFullYear()) {
      categories['в этом году'].push(task);
    } else {
      categories['в следующем году и более'].push(task);
    }
  });

  return categories;
}
