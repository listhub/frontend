import { ListItem, Summary } from '@/shared/types.ts';

export function countDoneItems(list: ListItem[]): Summary {
  let totalItems = 0;
  let doneItems = 0;

  function countItemsRecursive(items: ListItem[]) {
    for (const item of items) {
      if (item.children.length > 0) {
        countItemsRecursive(item.children);
      } else {
        totalItems++;
        if (item.isDone) {
          doneItems++;
        }
      }
    }
  }

  countItemsRecursive(list);

  return { totalItems, doneItems };
}
