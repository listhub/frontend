import React from 'react';

interface PressedKey {
  code: string;
  timestamp: number;
}

const pressedKeys: PressedKey[] = [];

function getKeyPress(code: string): string {
  pressedKeys.push({ code, timestamp: Date.now() });

  if (pressedKeys.length > 2) pressedKeys.shift();

  if (pressedKeys.length === 2) {
    const timeDifference = pressedKeys[1].timestamp - pressedKeys[0].timestamp;
    return timeDifference < 300
      ? pressedKeys.map(key => key.code).join('_')
      : pressedKeys[pressedKeys.length - 1].code;
  }

  return pressedKeys[0].code;
}

interface KeyboardEventHandlers {
  event: React.KeyboardEvent;
  postNewListItem: () => void;
  setShowNewItem: React.Dispatch<React.SetStateAction<boolean>>;
  setIsSubItem: React.Dispatch<React.SetStateAction<boolean>>;
  toggleItemActive?: () => void;
  handleDeleteItem?: () => void;
  setColor?: (colorId: number) => void;
  focusOnList?: () => void;
  changePosition?: (n: number) => void;
  setShowMenu?: React.Dispatch<React.SetStateAction<boolean>>;
  setShowPresetDate?: React.Dispatch<React.SetStateAction<boolean>>;
  setShowManageTag?: React.Dispatch<React.SetStateAction<boolean>>;
  setIsEditing?: React.Dispatch<React.SetStateAction<boolean>>;
  copyList?: () => void;
  pasteList?: () => void;
  moveToTopParentList?: () => void;
  moveToTopList?: () => void;
}

export const handleKeyPress = ({
  event,
  toggleItemActive,
  setShowNewItem,
  setShowMenu,
  setIsSubItem,
  postNewListItem,
  handleDeleteItem,
  setIsEditing,
  setShowManageTag,
  setShowPresetDate,
  changePosition,
  focusOnList,
  setColor,
  copyList,
  pasteList,
  moveToTopParentList,
  moveToTopList,
}: KeyboardEventHandlers) => {
  const { code, shiftKey, target } = event;

  const keyPressed = getKeyPress(code);

  const isSpace = code === 'Space';
  const isEnter = code === 'Enter';
  const isEscape = code === 'Escape';
  const isDelete = code === 'Delete';
  const isArrowUp = code === 'ArrowUp';
  const isArrowDown = code === 'ArrowDown';
  const isShift = shiftKey;
  const isTargetDiv = target instanceof HTMLDivElement;
  const isTargetTextArea = target instanceof HTMLTextAreaElement;

  if (isArrowDown) {
    if (
      keyPressed !== 'ControlLeft_ArrowDown' &&
      keyPressed !== 'ControlLeft_ArrowUp' &&
      keyPressed !== 'ControlRight_ArrowDown' &&
      keyPressed !== 'ControlRight_ArrowUp'
    ) {
      const tabs = window.document.querySelectorAll('[tabIndex]');
      const index = Array.from(tabs).indexOf(event.target as Element);
      (tabs[index + 1] as HTMLElement)?.focus();
    }
  }
  if (isArrowUp) {
    if (
      keyPressed !== 'ControlLeft_ArrowDown' &&
      keyPressed !== 'ControlLeft_ArrowUp' &&
      keyPressed !== 'ControlRight_ArrowDown' &&
      keyPressed !== 'ControlRight_ArrowUp'
    ) {
      const tabs = window.document.querySelectorAll('[tabIndex]');
      const index = Array.from(tabs).indexOf(event.target as Element);
      (tabs[index - 1] as HTMLElement)?.focus();
    }
  }

  if (
    keyPressed === 'ControlLeft_ArrowUp' ||
    keyPressed === 'ControlRight_ArrowUp'
  ) {
    changePosition && changePosition(-1);
  }

  if (
    keyPressed === 'ControlLeft_ArrowDown' ||
    keyPressed === 'ControlRight_ArrowDown'
  ) {
    changePosition && changePosition(1);
  }

  if (
    keyPressed === 'ShiftLeft_ArrowRight' ||
    keyPressed === 'ShiftRight_ArrowRight'
  ) {
    focusOnList && focusOnList();
  }

  if (isSpace && toggleItemActive) {
    toggleItemActive();
  }
  if (isDelete) {
    handleDeleteItem && handleDeleteItem();
  }

  if (isEnter && isTargetDiv) {
    if (isShift) {
      setIsSubItem(true);
      event.preventDefault();
      setShowNewItem(true);
      return;
    }
    event.preventDefault();
    setShowNewItem(true);
  }

  if (isEnter && isTargetTextArea && !isShift) {
    event.preventDefault();
    postNewListItem();
  }

  if (isEscape) {
    setIsSubItem(false);
    setShowNewItem(false);
  }

  if (code === 'F2' || keyPressed === 'KeyE_KeyE') {
    setIsEditing && setIsEditing(true);
  }

  if (keyPressed === 'KeyT_KeyT') {
    setShowMenu && setShowMenu(true);
    setTimeout(() => {
      setShowManageTag && setShowManageTag(true);
    }, 0);
  }

  if (keyPressed === 'KeyD_KeyD') {
    setShowMenu && setShowMenu(true);
    setTimeout(() => {
      setShowPresetDate && setShowPresetDate(true);
    }, 0);
  }

  if (keyPressed === 'KeyA_KeyA') {
    setShowMenu && setShowMenu(true);
  }

  if (keyPressed === 'ControlLeft_KeyC') {
    copyList && copyList();
  }

  if (keyPressed === 'ControlLeft_KeyV') {
    pasteList && pasteList();
  }

  if (code === 'Escape') {
    setShowMenu && setShowMenu(false);
    setShowManageTag && setShowManageTag(false);
  }

  if (keyPressed === 'Tab') {
    event.preventDefault();
    moveToTopList && moveToTopList();
  }

  if (keyPressed === 'ShiftLeft_Tab') {
    event.preventDefault();
    moveToTopParentList && moveToTopParentList();
  }

  if (setColor) {
    const colorMap: Record<string, number> = {
      Digit0: 0,
      Digit1: 1,
      Digit2: 2,
      Digit3: 3,
      Digit4: 4,
      Digit5: 5,
      Digit6: 6,
      Digit7: 7,
      Digit8: 8,
    };

    if (code in colorMap) {
      setColor(colorMap[code]);
    }
  }
};
