import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ListState } from '@/shared/types';

const LIST_STATE: ListState = {
  items: null,
  options: {
    hideCompleted: false,
    moveCompletedDown: false,
    showDetails: false,
    showProgressCounter: false,
    listStyle: 'number',
  },
};

const listSlice = createSlice({
  name: 'lists',
  initialState: LIST_STATE,
  reducers: {
    setHideCompleted(state, action: PayloadAction<boolean>) {
      state.options.hideCompleted = action.payload;
    },
    setMoveCompletedDown(state, action: PayloadAction<boolean>) {
      state.options.moveCompletedDown = action.payload;
    },
    setShowDetails(state, action: PayloadAction<boolean>) {
      state.options.showDetails = action.payload;
    },
    setShowProgressCounter(state, action: PayloadAction<boolean>) {
      state.options.showProgressCounter = action.payload;
    },
    setListStyle(state, action) {
      state.options.listStyle = action.payload;
    },
  },
});

export const {
  setHideCompleted,
  setMoveCompletedDown,
  setShowDetails,
  setShowProgressCounter,
  setListStyle,
} = listSlice.actions;

export default listSlice.reducer;
