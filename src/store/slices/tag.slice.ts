import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const TAG_STATE = {
  isEditing: false,
};

const tagSlice = createSlice({
  name: 'tags',
  initialState: TAG_STATE,
  reducers: {
    setTagIsEditing(state, action: PayloadAction<boolean>) {
      state.isEditing = action.payload;
    },
  },
});

export const { setTagIsEditing } = tagSlice.actions;

export default tagSlice.reducer;
