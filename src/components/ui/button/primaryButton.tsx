import cn from 'classnames';
import { ButtonProps } from './types';
import styles from './button.module.css';

const PrimaryButton = ({
  text,
  onClick,
  className,
  icon,
  ...props
}: ButtonProps) => {
  return (
    <button
      className={cn(styles.button, styles.primaryButton, className)}
      onClick={onClick}
      {...props}
    >
      {text}
      {icon}
    </button>
  );
};

export default PrimaryButton;
