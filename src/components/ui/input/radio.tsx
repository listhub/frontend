import { InputHTMLAttributes, forwardRef } from 'react';
import cn from 'classnames';
import styles from './input.module.css';

interface RadioInputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const RadioInput = forwardRef<HTMLInputElement, RadioInputProps>(
  ({ label, name, value, className = '', checked, onChange, ...rest }, ref) => {
    const handleClick = () => {
      if (onChange) {
        onChange({ target: { value } } as React.ChangeEvent<HTMLInputElement>);
      }
    };

    return (
      <label
        htmlFor={`radio_${value}`}
        className={cn(styles.radio, className)}
        onClick={handleClick}
      >
        <input
          {...rest}
          value={value}
          type="radio"
          name={name}
          ref={ref}
          className={styles.input}
          id={`radio_${value}`}
          checked={checked}
          onChange={onChange}
        />
        {label}
      </label>
    );
  },
);

RadioInput.displayName = 'Input';

export default RadioInput;
