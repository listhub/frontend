import React from 'react';
import { Link } from 'react-router-dom';
import { URLManager } from '@/config/url.config';
import styles from './logo.module.css';

interface LogoProps {
  className?: string;
}

const Logo: React.FC<LogoProps> = ({ className }) => {
  return (
    <Link to={URLManager.homeURL} className={`${styles.logo} ${className}`}>
      List<span>Hub</span>
    </Link>
  );
};

export default Logo;
