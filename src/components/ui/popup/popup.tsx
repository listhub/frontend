import React, { Dispatch, FC, SetStateAction } from 'react';
import styles from './popup.module.css';
import Portal from './portal';

const Popup: FC<{
  children: React.ReactNode;
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
}> = ({ children, isOpened, setIsOpened }) => {
  if (!isOpened) return null;

  return (
    <Portal>
      <div className={styles.popup} role="dialog">
        <div
          className={styles.overlay}
          role="button"
          tabIndex={0}
          onClick={() => setIsOpened(!isOpened)}
        />
        {children}
      </div>
    </Portal>
  );
};

export default Popup;
