import PrimaryButton from '../../button/primaryButton';
import SecondaryButton from '../../button/secondaryButton';
import Popup from '../popup';
import styles from './dialog.module.css';

interface DialogProps {
  isOpened: boolean;
  onClose: () => void;
  header: string;
  text: string;
  primaryButtonText: string;
  primaryButtonOnClick: () => void;
  secondaryButtonText: string;
  secondaryButtonOnClick: () => void;
}

const Dialog = ({
  isOpened,
  onClose,
  header,
  text,
  primaryButtonText,
  primaryButtonOnClick,
  secondaryButtonText,
  secondaryButtonOnClick,
}: DialogProps) => {
  return (
    <Popup isOpened={isOpened} setIsOpened={onClose}>
      <div className={styles.dialog}>
        <div className={styles.header}>{header}</div>
        <div className={styles.text}>{text}</div>
        <div className={styles.buttons}>
          <SecondaryButton
            text={secondaryButtonText}
            onClick={secondaryButtonOnClick}
          />
          <PrimaryButton
            text={primaryButtonText}
            onClick={primaryButtonOnClick}
          />
        </div>
      </div>
    </Popup>
  );
};

export default Dialog;
