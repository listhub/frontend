import { useState } from 'react';
import { VscChevronDown, VscChevronRight } from 'react-icons/vsc';
import { ListItem } from '@/shared/types';
import { formatDateToLocale } from '@/shared/utils/datetime.utils';
import styles from './dates.module.css';
import { Link } from 'react-router-dom';
import { URLManager } from '@/config/url.config';
import Breadcrumbs from '../list/breadcrumbs';

const Category = ({
  category,
  lists,
}: {
  category: string;
  lists: ListItem[];
}) => {
  const [show, setShow] = useState(true);

  if (lists.length === 0) return;

  return (
    <div className={styles.category}>
      <div className={styles.categoryTitle}>
        <span className={styles.handleShow} onClick={() => setShow(!show)}>
          {show ? <VscChevronDown /> : <VscChevronRight />}
        </span>
        {category[0].toUpperCase() + category.substring(1)}
      </div>
      <div className={styles.taskList}>
        {show &&
          lists.map((list, index) => (
            <div key={list.id} className={styles.taskItem}>
              <span>{index + 1}</span>&nbsp;
              <Link to={URLManager.list(list.id)}>{list.title}</Link>&nbsp;
              <span className={styles.presetDate}>
                {list.presetDate && formatDateToLocale(list.presetDate)}
              </span>
              <Breadcrumbs id={list.id} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default Category;
