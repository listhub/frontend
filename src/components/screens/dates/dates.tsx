import Layout from '@/components/layout/layout';
import { listAPI } from '@/services/list.service';
import { sortTasksByTime } from '@/shared/utils/sortListsByTime.utils';
import Category from './category';
import styles from './dates.module.css';

const Dates = () => {
  const { data: lists } = listAPI.useGetListsQuery({});
  const listWithPresetDate = lists?.filter(list => list.presetDate);

  const sortingListByPresetDate = sortTasksByTime(listWithPresetDate);

  return (
    <Layout>
      <div className={styles.header}>
        <div className={styles.links}>
          <span className={styles.active}>Интервалы</span>
          <span className={styles.timing}>Повторяющиеся</span>
        </div>
      </div>

      <div className={styles.container}>
        {sortingListByPresetDate &&
          Object.entries(sortingListByPresetDate).map(([category, lists]) => (
            <Category category={category} lists={lists} key={category} />
          ))}
      </div>
    </Layout>
  );
};

export default Dates;
