import { ChangeEvent, useState } from 'react';
import { Link } from 'react-router-dom';
import { BsThreeDotsVertical } from 'react-icons/bs';
import { HiMiniPencilSquare } from 'react-icons/hi2';
import { IoMdColorFill } from 'react-icons/io';
import { FaTrash } from 'react-icons/fa';
import { Tag } from '@/shared/types';
import { getContrastColor } from '@/shared/utils/getContrastColor.utils';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import RemoveTag from '@/components/ui/popup/dialog/removeTag';
import { setTagIsEditing } from '@/store/slices/tag.slice.ts';
import { URLManager } from '@/config/url.config';
import { useAppDispatch } from '@/shared/hooks';
import {
  useDeleteTagMutation,
  useUpdateTagMutation,
} from '@/services/tag.service';
import styles from './tags.module.css';

const RenderTagItem = ({ tag }: { tag: Tag }) => {
  const [showMenu, setShowMenu] = useState(false);
  const [openedPopup, setOpenedPopup] = useState(false);
  const [showLists, setShowLists] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [text, setText] = useState(tag.title);
  const [fillColor, setFillColor] = useState(tag.color);

  const [deleteTag, { isLoading: isDeleteLoading }] = useDeleteTagMutation();
  const [updateTag, { isLoading: isUpdateLoading }] = useUpdateTagMutation();
  const dispatch = useAppDispatch();

  if (isUpdateLoading || isDeleteLoading) {
    dispatch(setTagIsEditing(true));
  }

  const handleDeleteTag = async () => {
    await deleteTag(tag.id);
  };

  const handleTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };
  const handleColorChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFillColor(event.target.value);
  };
  const handleBlur = () => {
    setIsEdit(false);
    if (text !== tag.title || fillColor !== tag.color) {
      updateTag({ id: tag.id, title: text, color: fillColor });
    }
  };
  const clickOutside = () => {
    handleBlur();
    setShowMenu(false);
  };

  return (
    <>
      <div className={styles.tagCard}>
        <div className={styles.treeDots}>
          <BsThreeDotsVertical onClick={() => setShowMenu(!showMenu)} />
          <ClickOutside isShow={showMenu} setIsShow={clickOutside}>
            <div className={styles.tooltip}>
              <div className={styles.tooltipItem}>
                <IoMdColorFill />
                Заливка
                <input
                  className={styles.colorPicker}
                  type="color"
                  id="fillColor"
                  name="fillColor"
                  value={fillColor}
                  onChange={handleColorChange}
                  onBlur={handleBlur}
                />
              </div>
              <div
                className={styles.tooltipItem}
                onClick={() => {
                  setShowMenu(false);
                  setIsEdit(true);
                }}
              >
                <HiMiniPencilSquare />
                Редактировать
              </div>
              <div
                className={styles.tooltipItem}
                onClick={() => {
                  setShowMenu(false);
                  setOpenedPopup(true);
                }}
              >
                <FaTrash />
                Удалить
              </div>
            </div>
          </ClickOutside>
        </div>

        {!isEdit && (
          <div
            className={styles.tagTitle}
            style={{
              backgroundColor: tag.color,
              color: getContrastColor(tag.color),
            }}
          >
            #{tag.title}
          </div>
        )}
        {isEdit && (
          <input
            className={styles.tagTitle}
            type="text"
            value={text}
            onChange={handleTitleChange}
            onBlur={handleBlur}
            autoFocus={true}
          />
        )}

        <div
          className={styles.toggleList}
          onClick={() => setShowLists(!showLists)}
        >
          ×{tag.lists ? tag.lists.length : 0}
        </div>
      </div>

      {showLists &&
        tag.lists &&
        tag.lists.map((list, index) => (
          <div className={styles.listLink} key={list.id}>
            <span>{index + 1}.&nbsp;</span>
            <Link to={URLManager.list(list.id)}>{list.title}</Link>
          </div>
        ))}

      <RemoveTag
        isOpened={openedPopup}
        onClose={() => setOpenedPopup(false)}
        onOk={handleDeleteTag}
      />
    </>
  );
};

export default RenderTagItem;
