import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { VscChevronDown, VscChevronRight } from 'react-icons/vsc';
import { BsThreeDotsVertical } from 'react-icons/bs';
import cn from 'classnames';
import EditableTextItem from '@/components/ui/editableTextItem/editableTextItem';
import { colorPalette } from '@/components/screens/list/menu/presetColor.tsx';
import { getContrastColor } from '@/shared/utils/getContrastColor.utils';
import { formatDateToLocale } from '@/shared/utils/datetime.utils';
import { handleKeyPress } from '@/shared/utils/keypress.utils';
import { generateMarkdownList } from '@/shared/utils/markdown.utils.ts';
import { useAppSelector } from '@/shared/hooks';
import {
  listAPI,
  useCreateListMutation,
  useDeleteListMutation,
  useDuplicateListMutation,
  useGetParentListsQuery,
  useUpdateListMutation,
} from '@/services/list.service';
import { ListItem } from '@/shared/types.ts';
import { TreeListProps } from './types';
import Menu from './menu/menu';
import styles from './list.module.css';

const TreeList: React.FC<TreeListProps> = ({
  listItem,
  level,
  parentPath,
  showChildren = true,
}) => {
  const [show, setShow] = useState(showChildren);
  const [showMenu, setShowMenu] = useState(false);
  const [showNewItem, setShowNewItem] = useState(false);
  const [showManageTag, setShowManageTag] = useState(false);
  const [showManageColor, setShowManageColor] = useState(false);
  const [showPresetDate, setShowPresetDate] = useState(false);
  const [isSubItem, setIsSubItem] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [textareaValue, setTextareaValue] = useState('');

  const [createList] = useCreateListMutation();
  const [deleteList] = useDeleteListMutation();
  const [updateList] = useUpdateListMutation();

  const navigate = useNavigate();

  const { hideCompleted, listStyle, showDetails } = useAppSelector(
    state => state.lists.options,
  );

  const itemPath =
    parentPath === undefined ? String(level) : `${parentPath}.${level}`;

  const toggleItemActive = () => {
    !isEditing &&
      !showNewItem &&
      !showMenu &&
      updateList({ id: listItem.id, isDone: !listItem.isDone });
  };

  const handleTitleChange = (newTitle: string) => {
    if (listItem && newTitle !== listItem.title) {
      updateList({ id: listItem.id, title: newTitle });
    }
  };

  const handleTextareaChange = (
    event: React.ChangeEvent<HTMLTextAreaElement>,
  ) => {
    setTextareaValue(event.target.value);
  };

  const postNewListItem = () => {
    if (!textareaValue.trim()) {
      setShowNewItem(false);
      return;
    }

    if (isSubItem) {
      createList({
        title: textareaValue,
        parentId: listItem.id,
        position: listItem.position + 1,
      });
    } else {
      createList({
        title: textareaValue,
        parentId: listItem.parentId,
        position: listItem.position + 1,
      });
    }

    setTextareaValue('');
  };

  const handleDeleteItem = () => {
    deleteList(listItem.id);
  };

  const setColor = (colorId: number) => {
    !isEditing &&
      !showNewItem &&
      !showMenu &&
      updateList({
        id: listItem.id,
        color: colorPalette[colorId],
      });
  };

  const changePosition = (n: number) => {
    updateList({ id: listItem.id, position: listItem.position + n });
  };

  const [duplicateListMutation] = useDuplicateListMutation();

  const copyList = () => {
    localStorage.setItem('copyListId', listItem.id);
    navigator.clipboard.writeText(generateMarkdownList(listItem));
  };

  const { data: parentLists } = useGetParentListsQuery({
    id: listItem.id,
  });
  const allList = listAPI.useGetListByIdQuery({
    id: (parentLists && parentLists[parentLists.length - 1].id) || '',
  });

  const pasteList = async () => {
    const copying = localStorage.getItem('copyListId') || '';
    if (
      parentLists &&
      parentLists.some(parentList => parentList.id === copying)
    ) {
      return alert(
        'Пока нет возможности вствалять родительский элемент в дочерний, но мы уже работаем над этим :)',
      );
    }
    await duplicateListMutation({
      id: copying,
      parentId: listItem.id,
    });
    // localStorage.removeItem('copyListId');
  };

  const findItemById = (tree: ListItem, id: string | null): ListItem | null => {
    if (tree.id === id) {
      return tree;
    }

    if (tree.children && tree.children.length > 0) {
      for (const child of tree.children) {
        const result = findItemById(child, id);
        if (result) {
          return result;
        }
      }
    }
    return null;
  };
  const moveToTopList = async () => {
    if (allList.data) {
      const parent = findItemById(allList.data, listItem.parentId);
      const childrenWithLowerPosition = parent?.children
        .filter((child: ListItem) => child.position < listItem.position)
        .sort((a, b) => a.position - b.position)[0];
      if (childrenWithLowerPosition) {
        await duplicateListMutation({
          id: listItem.id,
          parentId: childrenWithLowerPosition.id,
        }).then(() => deleteList(listItem.id));
      }
    }
  };
  const moveToTopParentList = async () => {
    if (!parentLists || parentLists.length === 1) return;
    const parentListId = parentLists[1].id;
    if (parentListId === null) return;
    await duplicateListMutation({
      id: listItem.id,
      parentId: parentListId,
    }).then(() => deleteList(listItem.id));
  };

  const handleKeyPressWrapper = (event: React.KeyboardEvent) => {
    handleKeyPress({
      event,
      toggleItemActive,
      postNewListItem,
      setShowNewItem,
      setShowMenu,
      setIsSubItem,
      setIsEditing,
      handleDeleteItem,
      setShowManageTag,
      changePosition,
      focusOnList,
      setShowPresetDate,
      setColor,
      copyList,
      pasteList,
      moveToTopParentList,
      moveToTopList,
    });
  };

  const focusOnList = () => {
    navigate(`/list/${listItem.id}`);
  };

  useEffect(() => {
    setShowManageTag(false);
    setShowManageColor(false);
    setShowPresetDate(false);
  }, [showMenu]);

  const fillColor = {
    backgroundColor: listItem.color || '#fff',
  };

  const dragHandlers = {
    onDragStart: () => localStorage.setItem('copyListId', listItem.id),

    onDragLeave: (event: React.DragEvent<HTMLDivElement>) => {
      event.currentTarget.style.backgroundColor = 'transparent';
    },

    onDragEnd: (event: React.DragEvent<HTMLDivElement>) => {
      event.currentTarget.style.backgroundColor = 'transparent';
    },

    onDragOver: (event: React.DragEvent<HTMLDivElement>) => {
      event.currentTarget.style.backgroundColor = 'var(--blue-00)';
      event.preventDefault();
      if (
        (parentLists &&
          parentLists.some(
            parentList => parentList.id === localStorage.getItem('copyListId'),
          )) ||
        listItem.id === localStorage.getItem('copyListId')
      ) {
        event.dataTransfer.dropEffect = 'none';
      }
    },

    onDrop: (event: React.DragEvent<HTMLDivElement>) => {
      event.preventDefault();
      event.currentTarget.style.backgroundColor = 'transparent';
      pasteList().then(() =>
        deleteList(localStorage.getItem('copyListId') || ''),
      );
    },
  };

  if (hideCompleted && listItem.isDone) return null;

  return (
    <div className={styles.treeListItem}>
      <div
        className={cn(styles.taskItem, {
          [styles.doneTask]: listItem.isDone,
        })}
        tabIndex={0}
        onKeyDown={handleKeyPressWrapper}
        style={fillColor}
        draggable
        {...dragHandlers}
      >
        {listItem.children.length > 0 && (
          <span className={styles.handleShow} onClick={() => setShow(!show)}>
            {show ? <VscChevronDown /> : <VscChevronRight />}
          </span>
        )}

        {listStyle === 'number' && <div>{itemPath}</div>}
        {listStyle === 'checkbox' && (
          <input
            type="checkbox"
            checked={listItem.isDone}
            onChange={toggleItemActive}
            className={styles.status}
            tabIndex={-1}
          />
        )}
        <EditableTextItem
          initialText={listItem.title}
          onSave={handleTitleChange}
          isEditMode={isEditing}
          setIsEditingMode={setIsEditing}
        />
        {listItem.tags.map(tag => (
          <div
            className={styles.tagItem}
            key={tag.id}
            style={{
              backgroundColor: tag.color,
              color: getContrastColor(tag.color),
            }}
          >
            #{tag.title}
          </div>
        ))}
        {!showPresetDate && listItem.presetDate && (
          <span className={styles.presetDate}>
            {formatDateToLocale(listItem.presetDate)}
          </span>
        )}
        {showDetails && (
          <span className={styles.details}>
            {formatDateToLocale(listItem.updateDate)}
          </span>
        )}
        <BsThreeDotsVertical
          className={styles.treeDots}
          onClick={() => setShowMenu(!showMenu)}
        />
        <Menu
          parentList={listItem}
          showMenu={showMenu}
          setShowMenu={setShowMenu}
          showManageTag={showManageTag}
          setShowManageTag={setShowManageTag}
          showManageColor={showManageColor}
          setShowManageColor={setShowManageColor}
          showPresetDate={showPresetDate}
          setShowPresetDate={setShowPresetDate}
          setShowNewItem={setShowNewItem}
          setIsSubItem={setIsSubItem}
          handleDeleteItem={handleDeleteItem}
          focusOnList={focusOnList}
          setIsEditing={setIsEditing}
        />
      </div>
      {show && listItem.children.length > 0 && (
        <div className={styles.childrenContainer}>
          {listItem.children
            .toSorted((a, b) => a.position - b.position)
            .map((child, index) => (
              <TreeList
                key={child.id}
                listItem={child}
                level={index + 1}
                parentPath={itemPath}
              />
            ))}
        </div>
      )}
      {showNewItem && (
        <textarea
          className={cn(styles.textarea, isSubItem && styles.subitem)}
          autoFocus={showNewItem}
          value={textareaValue}
          onChange={handleTextareaChange}
          onKeyDown={handleKeyPressWrapper}
          onBlur={postNewListItem}
        />
      )}
    </div>
  );
};

export default TreeList;
