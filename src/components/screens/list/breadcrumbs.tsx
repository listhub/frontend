import { useGetParentListsQuery } from '@/services/list.service';
import styles from './list.module.css';
import { URLManager } from '@/config/url.config';
import { Link } from 'react-router-dom';

const Breadcrumbs = ({ id }: { id: string }) => {
  const {
    data: parentLists,
    isLoading: isLoadingParentLists,
    error: isErrorParentLists,
  } = useGetParentListsQuery({ id: id || '' });
  return (
    <div className={styles.breadcrumbs}>
      {isLoadingParentLists && <span>Загрузка...</span>}
      {isErrorParentLists && <span>Произошла ошибка</span>}
      {parentLists &&
        parentLists.toReversed().map(list => (
          <span key={list.id}>
            <Link to={URLManager.list(list.id)}>{list.title}</Link>
            &nbsp;/&nbsp;
          </span>
        ))}
    </div>
  );
};

export default Breadcrumbs;
