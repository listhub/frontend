import styles from './progressBar.module.css';

interface ProgressBarProps {
  value: number;
  max: number;
}

const ProgressBar = ({ value, max }: ProgressBarProps) => {
  const progressPercentage = (value / max) * 100;

  return (
    <div className={styles.progressBar}>
      <div
        className={styles.filler}
        style={{ width: `${progressPercentage}%` }}
      />
      <div className={styles.progress}>
        <span>{value}</span>/<span>{max}</span>
      </div>
    </div>
  );
};

export default ProgressBar;
