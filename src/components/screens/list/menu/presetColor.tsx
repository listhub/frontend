import { useUpdateListMutation } from '@/services/list.service';
import styles from './menu.module.css';

export const colorPalette = [
  '#ffffff', // var(--background-white)
  '#ffe9e6', // var(--red-00)
  '#ffe0c3', // var(--orange-00)
  '#ffefd4', // var(--yellow-00)
  '#d7f8ae', // var(--green-00)
  '#dde6ff', // var(--blue-00)
  '#f7d7ff', // var(--purple-00)
  '#e8e9eb', // var(--background-dark-gray)
  '#f5f5f5', // var(--background-gray)
];

const PresetColor = ({ listId }: { listId: string }) => {
  const [updateList] = useUpdateListMutation();

  const setColor = (colorId: number) => {
    updateList({
      id: listId,
      color: colorPalette[colorId],
    });
  };

  return (
    <div className={styles.presetColorWrapper}>
      {Array.from({ length: 9 }, (_, i) => (
        <div key={i} className={styles.presetColor} onClick={() => setColor(i)}>
          {i}
        </div>
      ))}
    </div>
  );
};

export default PresetColor;
