import { Dispatch, SetStateAction } from 'react';
import { MdCenterFocusWeak, MdPlaylistAdd } from 'react-icons/md';
import { IoColorPaletteOutline } from 'react-icons/io5';
import { HiMiniPencilSquare } from 'react-icons/hi2';
import { CiCalendarDate } from 'react-icons/ci';
import { LiaTagsSolid } from 'react-icons/lia';
import { FaTrash } from 'react-icons/fa';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import styles from './menu.module.css';
import TagSelect from './tagsSelect';
import PresetDate from './presetDate';
import { ListItem } from '@/shared/types';
import PresetColor from './presetColor';

interface MenuProps {
  parentList: ListItem;
  showMenu: boolean;
  setShowMenu: Dispatch<SetStateAction<boolean>>;
  showManageTag: boolean;
  setShowManageTag: Dispatch<SetStateAction<boolean>>;
  showManageColor: boolean;
  setShowManageColor: Dispatch<SetStateAction<boolean>>;
  showPresetDate: boolean;
  setShowPresetDate: Dispatch<SetStateAction<boolean>>;
  setShowNewItem: Dispatch<SetStateAction<boolean>>;
  setIsSubItem: Dispatch<SetStateAction<boolean>>;
  setIsEditing: Dispatch<SetStateAction<boolean>>;
  handleDeleteItem: () => void;
  focusOnList: () => void;
}

const Menu = ({
  parentList,
  showMenu,
  setShowMenu,
  showManageTag,
  setShowManageTag,
  showManageColor,
  setShowManageColor,
  showPresetDate,
  setShowPresetDate,
  setShowNewItem,
  setIsSubItem,
  handleDeleteItem,
  focusOnList,
  setIsEditing,
}: MenuProps) => {
  const handleAddSubItem = () => {
    setShowNewItem(true);
    setIsSubItem(true);
    setShowMenu(false);
  };
  const handleEditTitle = () => {
    setIsEditing(true);
    setShowMenu(false);
  };

  if (showManageTag) {
    return (
      <ClickOutside isShow={showMenu} setIsShow={setShowMenu}>
        <TagSelect parentId={parentList.id} />
      </ClickOutside>
    );
  }
  if (showManageColor) {
    return (
      <ClickOutside isShow={showMenu} setIsShow={setShowMenu}>
        <PresetColor listId={parentList.id} />
      </ClickOutside>
    );
  }

  if (showPresetDate) {
    return (
      <ClickOutside isShow={showMenu} setIsShow={setShowMenu}>
        <PresetDate
          parentId={parentList.id}
          parentPresetDate={parentList.presetDate}
        />
      </ClickOutside>
    );
  }

  return (
    <ClickOutside isShow={showMenu} setIsShow={setShowMenu}>
      <div className={styles.tooltip}>
        <div className={styles.tooltipItem} onClick={focusOnList}>
          <MdCenterFocusWeak />
          <div className={styles.tooltipCommandTitle}>Сфокусировать</div>
          <div className={styles.hotkeyCommand}>Shift →</div>
        </div>
        <div className={styles.tooltipItem} onClick={handleAddSubItem}>
          <MdPlaylistAdd />
          <div className={styles.tooltipCommandTitle}>Добавить подпункт</div>
          <div className={styles.hotkeyCommand}>Shift + Enter</div>
        </div>
        <div className={styles.tooltipItem}>
          <LiaTagsSolid />
          <div
            className={styles.tooltipCommandTitle}
            onClick={() => setShowManageTag(true)}
          >
            Управление тегами
          </div>
          <div className={styles.hotkeyCommand}>tt</div>
        </div>
        <div className={styles.tooltipItem}>
          <CiCalendarDate />
          <div
            className={styles.tooltipCommandTitle}
            onClick={() => setShowPresetDate(true)}
          >
            Управление периодом
          </div>
          <div className={styles.hotkeyCommand}>dd</div>
        </div>
        <div className={styles.tooltipItem}>
          <IoColorPaletteOutline />
          <div
            className={styles.tooltipCommandTitle}
            onClick={() => setShowManageColor(true)}
          >
            Приоритет
          </div>
          <div className={styles.hotkeyCommand}>0</div>
          <div className={styles.hotkeyCommand}>1</div>
          ...
          <div className={styles.hotkeyCommand}>8</div>
        </div>
        <div className={styles.tooltipItem}>
          <HiMiniPencilSquare />
          <div className={styles.tooltipCommandTitle} onClick={handleEditTitle}>
            Переименовать
          </div>
          <div className={styles.hotkeyCommand}>ee</div>
          <div className={styles.hotkeyCommand}>F2</div>
        </div>
        <div className={styles.tooltipItem} onClick={handleDeleteItem}>
          <FaTrash />
          <div className={styles.tooltipCommandTitle}>Удалить</div>
          <div className={styles.hotkeyCommand}>del</div>
        </div>
        <div className={styles.tooltipItem}>
          Перейти ко всем горячим клавишам
        </div>
      </div>
    </ClickOutside>
  );
};

export default Menu;
