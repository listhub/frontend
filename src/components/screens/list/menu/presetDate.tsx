import React, { useState, useEffect } from 'react';
import { useUpdateListMutation } from '@/services/list.service';
import { parseDateStringToLocale } from '@/shared/utils/datetime.utils';

const PresetDate = ({
  parentId,
  parentPresetDate,
}: {
  parentId: string;
  parentPresetDate: string | null;
}) => {
  const [datetime, setDatetime] = useState<string>(
    parseDateStringToLocale(parentPresetDate || '').substring(0, 16) || '',
  );

  const [updateList] = useUpdateListMutation();

  useEffect(() => {
    setDatetime(parentPresetDate || '');
  }, [parentPresetDate]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const dt = event.target.value;

    setDatetime(dt);
    if (dt === '') {
      updateList({ id: parentId, presetDate: null });
    } else if (dt !== datetime) {
      updateList({ id: parentId, presetDate: new Date(dt).toISOString() });
    }
  };

  return (
    <input
      type="datetime-local"
      value={parseDateStringToLocale(datetime).substring(0, 16)}
      // value={datetime}
      onChange={handleChange}
    />
  );
};

export default PresetDate;
