import { useState } from 'react';
import { BsThreeDotsVertical } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';
import { LuFolder, LuFolderArchive } from 'react-icons/lu';
import { GoDuplicate } from 'react-icons/go';
import { SlTrash } from 'react-icons/sl';
import {
  useDeleteListMutation,
  useDuplicateListMutation,
  useUpdateListMutation,
} from '@/services/list.service';
import { formatDateToLocale } from '@/shared/utils/datetime.utils';
import RemoveList from '@/components/ui/popup/dialog/removeList';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import { ListItem } from '@/shared/types';
import { URLManager } from '@/config/url.config';
import styles from './table.module.css';

const TableRow = (tableItem: ListItem) => {
  const [opened, setOpened] = useState(false);
  const [openedPopup, setOpenedPopup] = useState(false);

  const navigate = useNavigate();
  const toListItemPage = () => {
    navigate(URLManager.list(tableItem.id));
  };

  const [deleteList] = useDeleteListMutation();
  const handleDelete = () => {
    deleteList(tableItem.id);
    setOpenedPopup(false);
  };
  const [updateList] = useUpdateListMutation();
  const setDone = (isArchived: boolean) => {
    updateList({ id: tableItem.id, isDone: isArchived });
    setOpenedPopup(false);
  };

  const [duplicateListMutation] = useDuplicateListMutation();
  const duplicateList = async () => {
    await duplicateListMutation({
      id: tableItem.id,
      parentId: null,
    });
  };

  return (
    <div className={styles.tableRow}>
      <div className={styles.tableRowItem} onClick={toListItemPage}>
        {tableItem.title}
      </div>
      <div className={styles.tableRowItem} onClick={toListItemPage}>
        {formatDateToLocale(tableItem.updateDate)}
      </div>
      <div className={styles.tableRowItem} onClick={toListItemPage}>
        unknown {/* tableItem.nestedCount */}
      </div>
      <div className={styles.tableRowItem} onClick={() => setOpened(!opened)}>
        <BsThreeDotsVertical />

        <ClickOutside isShow={opened} setIsShow={setOpened}>
          <div className={styles.tooltip}>
            {tableItem.isDone ? (
              <div
                className={styles.tooltipItem}
                onClick={() => setDone(false)}
              >
                <LuFolder />
                Активировать
              </div>
            ) : (
              <div className={styles.tooltipItem} onClick={() => setDone(true)}>
                <LuFolderArchive />
                Архивировать
              </div>
            )}

            <div className={styles.tooltipItem} onClick={duplicateList}>
              <GoDuplicate />
              Дублировать
            </div>

            <div
              className={styles.tooltipItem}
              onClick={() => setOpenedPopup(true)}
            >
              <SlTrash />
              Удалить
            </div>
          </div>
        </ClickOutside>
      </div>

      <RemoveList
        isOpened={openedPopup}
        onClose={() => setOpenedPopup(false)}
        onOk={handleDelete}
      />
    </div>
  );
};

export default TableRow;
