import cn from 'classnames';
import styles from './table.module.css';
import TableRow from './tableRow';
import { listAPI, useDuplicateListMutation } from '@/services/list.service';
import { ListItem } from '@/shared/types';
import { useEffect } from 'react';

const Table = ({
  listIsDoneParam,
  selectedSortValue,
}: {
  listIsDoneParam: boolean;
  selectedSortValue: string;
}) => {
  const {
    data: list,
    isLoading,
    error,
  } = listAPI.useGetListsQuery({
    parentId: null,
    isDone: listIsDoneParam,
  });

  const sortList = (list: ListItem[]) => {
    switch (selectedSortValue) {
      case 'time':
        return list?.toSorted(
          (a, b) =>
            new Date(b.updateDate).getTime() - new Date(a.updateDate).getTime(),
        );
      case 'alpha-asc':
        return list?.toSorted((a, b) => a.title.localeCompare(b.title));
      case 'alpha-desc':
        return list?.toSorted((a, b) => b.title.localeCompare(a.title));
      default:
        return list;
    }
  };

  const sortedList = list && sortList(list);

  const [duplicateListMutation] = useDuplicateListMutation();
  const pasteList = async () => {
    const copyingListId = localStorage.getItem('copyListId');
    if (copyingListId) {
      await duplicateListMutation({
        id: copyingListId,
        parentId: null,
      });
    }
  };

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === 'v') {
        pasteList();
      }
    };

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [pasteList]);

  return (
    <div className={styles.table}>
      <div className={cn(styles.tableRow, styles.tableHeader)}>
        <div className={styles.tableRowItem}>Список</div>
        <div className={styles.tableRowItem}>Последнее изменение</div>
        <div className={styles.tableRowItem}>Количество вложенностей</div>
        <div className={styles.tableRowItem}></div>
      </div>

      {isLoading && <span>Загрузка...</span>}
      {error && <span>Произошла ошибка</span>}
      {sortedList &&
        sortedList.map(item => <TableRow key={item.id} {...item} />)}
    </div>
  );
};

export default Table;
