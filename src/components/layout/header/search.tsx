import { useGetListsQuery } from '@/services/list.service';
import styles from './header.module.css';
import { Link } from 'react-router-dom';
import { URLManager } from '@/config/url.config';
import Breadcrumbs from '@/components/screens/list/breadcrumbs';

const Search = ({
  searchQuery,
  showBreadcrumb,
}: {
  searchQuery: string;
  showBreadcrumb: boolean;
}) => {
  const {
    data: lists,
    error,
    isLoading,
  } = useGetListsQuery({ title: searchQuery });

  return (
    <div className={styles.searchResult}>
      {isLoading && <div>Загрузка...</div>}
      {error && <div>Ошибка загрузки списков.</div>}
      {lists?.map(list => (
        <div key={list.id}>
          <Link to={URLManager.list(list.id)}>{list.title}</Link>
          {showBreadcrumb && <Breadcrumbs id={list.id} />}
        </div>
      ))}
      {!isLoading && !lists?.length && <div>Не найдено...</div>}
    </div>
  );
};

export default Search;
