import React, { useState } from 'react';
import { LuSettings2 } from 'react-icons/lu';
import { NavLink, useNavigate } from 'react-router-dom';
import SecondaryButton from '@/components/ui/button/secondaryButton';
import Logo from '@/components/ui/logo/logo';
import { URLManager } from '@/config/url.config';
import styles from './header.module.css';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import Search from './search';

const Header = () => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [showResult, setShowResult] = useState<boolean>(false);
  const [showSettings, setShowSettings] = useState(false);
  const [showBreadcrumb, setShowBreadcrumb] = useState(false);

  const navigate = useNavigate();

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setSearchQuery(event.target.value);
  };

  const handleInputFocus = () => {
    setShowResult(true);
  };

  const handleInputBlur = () => {
    setShowResult(false);
  };

  return (
    <div className={styles.header}>
      <Logo />

      <div className={styles.search}>
        <input
          type="text"
          value={searchQuery}
          onChange={handleSearchInputChange}
          onFocus={handleInputFocus}
          onClick={handleInputFocus}
          onBlur={handleInputBlur}
          placeholder="Поиск..."
        />
        <LuSettings2 onClick={() => setShowSettings(true)} />
        <ClickOutside isShow={showSettings} setIsShow={setShowSettings}>
          <div className={styles.settings}>
            <input
              type="checkbox"
              name="settings"
              id="settings"
              checked={showBreadcrumb}
              onChange={e => setShowBreadcrumb(e.target.checked)}
            />
            <label htmlFor="settings">Навигационная цепочка</label>
          </div>
        </ClickOutside>
        <ClickOutside isShow={showResult} setIsShow={setShowResult}>
          {!searchQuery && (
            <div className={styles.searchResult}>Введите запрос...</div>
          )}
          {searchQuery && (
            <Search searchQuery={searchQuery} showBreadcrumb={showBreadcrumb} />
          )}
        </ClickOutside>
      </div>

      <div className={styles.links}>
        <NavLink
          to={URLManager.homeURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Списки
        </NavLink>
        <NavLink
          to={URLManager.datesURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Сроки
        </NavLink>
        <NavLink
          to={URLManager.tagsURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Теги
        </NavLink>
      </div>

      <SecondaryButton
        text="Вход"
        onClick={() => navigate(URLManager.loginURL)}
        className={styles.auth}
        tabIndex={0}
      />
    </div>
  );
};

export default Header;
