import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Tag, TagData } from '@/shared/types';

const BASE_URL = '/api';

export const tagAPI = createApi({
  reducerPath: 'tagAPI',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders(headers) {
      return headers;
    },
    credentials: 'include',
  }),
  tagTypes: ['Tag'],
  endpoints: build => ({
    createTag: build.mutation<Tag, TagData>({
      query: tagData => ({
        url: '/tag',
        method: 'POST',
        body: tagData,
      }),
      invalidatesTags: ['Tag'],
    }),

    getTagById: build.query<Tag, string>({
      query: id => ({
        url: `/tag/${id}`,
      }),
      providesTags: ['Tag'],
    }),

    getAllTags: build.query<Tag[], void>({
      query: () => ({
        url: '/tag',
      }),
      providesTags: ['Tag'],
    }),

    updateTag: build.mutation<Tag, Tag>({
      query: ({ id, ...tagData }) => ({
        url: `/tag/${id}`,
        method: 'PUT',
        body: tagData,
      }),
      invalidatesTags: ['Tag'],
    }),

    deleteTag: build.mutation<void, string>({
      query: id => ({
        url: `/tag/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Tag'],
    }),
  }),
});

export const {
  useCreateTagMutation,
  useGetTagByIdQuery,
  useGetAllTagsQuery,
  useUpdateTagMutation,
  useDeleteTagMutation,
} = tagAPI;
