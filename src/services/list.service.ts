import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { ListItem, ListItemTitleParentId, Tag } from '@/shared/types';

const BASE_URL = '/api';

export const listAPI = createApi({
  reducerPath: 'listAPI',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders(headers) {
      return headers;
    },
    credentials: 'include',
  }),
  tagTypes: ['List'],
  endpoints: build => ({
    createList: build.mutation<ListItem, ListItemTitleParentId>({
      query: listData => ({
        url: '/list',
        method: 'POST',
        body: listData,
      }),
      invalidatesTags: ['List'],
    }),

    getListById: build.query<
      ListItem,
      {
        id: string;
      }
    >({
      query: ({ id }) => ({
        url: `/list/${id}`,
      }),
      providesTags: ['List'],
    }),

    getLists: build.query<
      ListItem[],
      {
        parentId?: string | null;
        title?: string;
        isDone?: boolean;
        color?: string;
        tags?: string[];
      }
    >({
      query: ({ parentId, title, isDone, color, tags }) => ({
        url: '/list',
        params: {
          parentId,
          title,
          isDone,
          color,
          tags,
        },
      }),
      providesTags: ['List'],
    }),

    updateList: build.mutation<
      ListItem,
      {
        id: string;
        title?: string;
        parentId?: string | null;
        isDone?: boolean;
        color?: string;
        tags?: string[];
        presetDate?: string | null;
        position?: number;
      }
    >({
      query: ({ id, ...listData }) => ({
        url: `/list/${id}`,
        method: 'PUT',
        body: listData,
      }),
      invalidatesTags: ['List'],
    }),

    deleteList: build.mutation<void, string>({
      query: id => ({
        url: `/list/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['List'],
    }),

    getParentLists: build.query<
      ListItem[],
      {
        id: string;
      }
    >({
      query: ({ id }) => ({
        url: `/list/${id}/parents`,
      }),
      providesTags: ['List'],
    }),

    addTagToList: build.mutation<ListItem, { listId: string; tagId: string }>({
      query: ({ listId, tagId }) => ({
        url: `/list/${listId}/tag/${tagId}`,
        method: 'POST',
      }),
      invalidatesTags: ['List'],
    }),

    removeTagFromList: build.mutation<
      ListItem,
      { listId: string; tagId: string }
    >({
      query: ({ listId, tagId }) => ({
        url: `/list/${listId}/tag/${tagId}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['List'],
    }),

    getTagsByListId: build.query<
      Tag[],
      {
        id: string;
      }
    >({
      query: ({ id }) => ({
        url: `/list/${id}/tags`,
      }),
      providesTags: ['List'],
    }),

    duplicateList: build.mutation<
      ListItem,
      {
        id: string;
        parentId?: string | null;
      }
    >({
      query: ({ id, parentId }) => ({
        url: `/list/${id}/duplicate`,
        method: 'POST',
        body: { parentId },
      }),
      invalidatesTags: ['List'],
    }),
  }),
});

export const {
  useCreateListMutation,
  useGetListByIdQuery,
  useGetListsQuery,
  useUpdateListMutation,
  useDeleteListMutation,
  useGetParentListsQuery,
  useAddTagToListMutation,
  useRemoveTagFromListMutation,
  useGetTagsByListIdQuery,
  useDuplicateListMutation,
} = listAPI;
